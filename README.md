# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Software (R scripts) and instructions that implement the methods of 
* "Modeling bias and variation in the stochastic processes of small RNA sequencing"
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Any standard version of R (including Microsoft R open) may be used to run the code
* Dependencies: execution of the software depends on the GAMLSS R package (www.gamlss.org), the TMB R package and the R development tools for your operating system and R version 
* If you would like to use the methods of our paper clone the folder BaseCode locally
* If you would like to reproduce the results of our paper, clone the folder NAR locally
* Compilation of C++ code to execute the TMB analyses IS required. We provide DLLs for a specific platform (64bit Win10) and 3.3.x versions of R, but these may not work under 32-bit versions or even earlier versions of R (and they most definitely will not work for non-Windows platforms)

### Who do I talk to? ###

* Christos Argyropoulos (argchris@hotmail.com)