## load datasets for the FDR analyses - based on the equimolar dataset
library(gamlss)
library(DESeq2)
library(rnaseqGene)
library(mgcv)
library(ggplot2)
library(mclust) ## for mixture model KDE
library(edgeR)
library(plyr)
library(limma)
library(DSS)
library(EBSeq)
library(TMB)
library("splitstackshape") ## for stratified sampling
rm(list=ls()) ## start with a clean slate
source("LQNO.R")
load("286.RData") ## load the v2, to generate the v2 image files
dyn.load(dynlib("LQNO_DE"))
dyn.load(dynlib("NBI_DE"))

## functions that does the fitting lifting
fitNoDelta<-function(x,dat,descr="")
{

  dat=merge(dat,x)

  ## Prepare data for the DeSEQ2 and the other programs that expect a sequence count
  ## and a design matrix as input. For each program specific data structures are
  ## created prior to calling the fitting functions
  datRatmat<-reshape(subset(dat,select=c(miRNA,reads,SampleID)),direction="wide",
                     v.names="reads",timevar="SampleID",idvar="miRNA")
  rownames(datRatmat)<-datRatmat[,1]
  datRatmat<-as.matrix(datRatmat[,-1])
  colData<-unique(subset(dat,select=c(SampleID,Amount,Series)))
  colData$SampleID<-paste("reads",as.character(colData$SampleID),sep=".")
  rownames(colData)<-colData[,1]
  colData<-colData[,-1]
  design<-model.matrix(~colData$Series) ## design matrix
  ## unfortunately need to have the data to the global environment
  assign("datRat0", dat, envir = .GlobalEnv) 
  attach(datRat0)
  ## to use the gamlss object - probably a weird way of constructing the call
  ## to gamlss
  t00<-Sys.time()
  fit1<-gamlss(reads~ Series+re(random=list(miRNA=pdIdent(~Series)),method="REML",
                                returnObject=TRUE,maxIter=5000,msMaxIter=5000,
                                niterEM=30,apVar=FALSE),
               sigma.fo=~ Series+re(random=list(miRNA=pdIdent(~Series)),method="REML",
                                    returnObject=TRUE,maxIter=5000,msMaxIter=5000,
                                    niterEM=30,apVar=FALSE),
               data=datRat0,family="NBI",
               control=gamlss.control(n.cyc=5000,c.crit=0.001,trace=TRUE,gd.tol=Inf),
               method=RS())
  gamlssfit<-ranef(getSmo(fit1))
  gamlssfit$miRNA<-row.names(gamlssfit)
  gamlssfit<-gamlssfit[,c(3,2)]
  names(gamlssfit)[2]<-"gamlss"
  gamlssfit<-transform(gamlssfit,gamlss=gamlss+fit1$mu.coefficients[2])
  t01<-Sys.time()
  t1<-Sys.time()
  u_X<-as.numeric(factor(datRat0$miRNA))
  u_G<-as.numeric(factor(datRat0$Series))
  y=datRat0$reads
  X<-model.matrix(~Series,data=datRat0)
  sigmu=rep(1,max(u_G))
  sigsig=rep(1,max(u_G))
  b=rep(0,ncol(X))
  s_b=rep(0,ncol(X))
  u_m=matrix(0,max(u_X),max(u_G))
  u_s=matrix(0,max(u_X),max(u_G))
  mumean=rep(0,max(u_G)-1)
  sigmamean=rep(0,max(u_G)-1)
  sigmaGroupMean=1;
  sigmaGroupSigma=1;
  obj.TMB<-MakeADFun(data=list(y=y,X=X,u_X=u_X,u_G=u_G),
                     parameters=list(b=b,s_b=s_b,u_m=u_m,u_s=u_s,
                                     sigmu=sigmu,sigsig=sigsig),
                     DLL="NBI_DE",random=c("u_m","u_s"),hessian=FALSE,silent=TRUE,
                     method="BFGS",random.start=expression(last.par[random]),
                     ADReport=TRUE)
  f.TMB<-nlminb(obj.TMB$par,obj.TMB$fn,obj.TMB$gr,
              control=list(eval.max=10000,iter.max=10000),lower=-30,upper=30)
  rep<-sdreport(obj.TMB)

  ## transform the coefficient and the covarience matrix to obtain differential
  ## expression levels in the second group. 
  dummy<-summary(rep,"report")[1:nlevels(datRat0$miRNA),"Estimate"]
  
  gamlssfit2<-data.frame(miRNA=levels(datRat0$miRNA),gamlssAD=dummy)
  t2<-Sys.time()
  ######################################################################################
  t3<-Sys.time()
  ## now use DESeq2
  dds <- DESeqDataSetFromMatrix(countData = datRatmat, colData = colData, design = ~ Series) 
  dds <- DESeq(dds) 
  res <- results(dds)
  t4<-Sys.time()
  ######################################################################################
  ## now use EdgeR
  t5<-Sys.time()
  EdgeRdata<-DGEList(datRatmat,group=as.numeric(colData$Series))
  EdgeRdata.TMM <- calcNormFactors(EdgeRdata)
  EdgeRdata.TMM <- estimateDisp(EdgeRdata.TMM, design,trend.method = "loess",robust=TRUE)
  EdgeR<-glmQLFit(EdgeRdata.TMM,design=design)
  edgeRfit<-data.frame(miRNA=row.names(EdgeR$coefficients),edgeR=EdgeR$coefficients[,2])
  t6<-Sys.time()
  ######################################################################################
  ## limma-trend with TMM
  t7<-Sys.time()
  logCPM.TMM<-cpm(EdgeRdata.TMM,log=TRUE,prior.count=3)
  limtr.TMM<-lmFit(logCPM.TMM,design=design)
  limtr.TMM <- eBayes(limtr.TMM, trend=TRUE)
  limtrfit.TMM<-as.data.frame(treat(limtr.TMM)$coef)
  limtrfit.TMM$miRNA<-rownames(limtrfit.TMM)
  limtrfit.TMM<-limtrfit.TMM[,c(3,2)]
  names(limtrfit.TMM)[2]<-"limmaTMM"
  t8<-Sys.time()
  ######################################################################################
  ## limma-trend without TMM
  t9<-Sys.time()
  logCPM<-cpm(EdgeRdata,log=TRUE,prior.count=3)
  limtr<-lmFit(logCPM,design=design)
  limtr <- eBayes(limtr, trend=TRUE)
  limtrfit<-as.data.frame(treat(limtr)$coef)
  limtrfit$miRNA<-rownames(limtrfit)
  limtrfit<-limtrfit[,c(3,2)]
  names(limtrfit)[2]<-"limma"
  t10<-Sys.time()
  ######################################################################################
  ## voom (applies TMM)
  t11<-Sys.time()
  voomData.TMM<-voom(EdgeRdata.TMM,design,plot=FALSE)
  vm.TMM<-lmFit(voomData.TMM,design=design)
  vm.TMM <- eBayes(vm.TMM, trend=TRUE)
  vmfit.TMM<-as.data.frame(treat(vm.TMM)$coef)
  vmfit.TMM$miRNA<-rownames(vmfit.TMM)
  vmfit.TMM<-vmfit.TMM[,c(3,2)]
  names(vmfit.TMM)[2]<-"voomTMM"
  t12<-Sys.time()
  ######################################################################################
  ## voom without normalization
  t13<-Sys.time()
  voomData<-voom(EdgeRdata,design,plot=FALSE)
  vm<-lmFit(voomData,design=design)
  vm <- eBayes(vm, trend=TRUE)
  vmfit<-as.data.frame(treat(vm)$coef)
  vmfit$miRNA<-rownames(vmfit)
  vmfit<-vmfit[,c(3,2)]
  names(vmfit)[2]<-"voom"
  t14<-Sys.time()
  ######################################################################################
  ## voom without normalization but with sample weights
  t15<-Sys.time()
  voomData.SW<-voomWithQualityWeights(EdgeRdata,design,plot=FALSE,norm="none")
  vm.SW<-lmFit(voomData.SW,design=design)
  vm.SW <- eBayes(vm.SW, trend=TRUE)
  vmfit.SW<-as.data.frame(treat(vm.SW)$coef)
  vmfit.SW$miRNA<-rownames(vmfit.SW)
  vmfit.SW<-vmfit.SW[,c(3,2)]
  names(vmfit.SW)[2]<-"voomSW"
  t16<-Sys.time()
  ######################################################################################
  ## DSS now
  t17<-Sys.time()
  dssdata<-datRatmat;colnames(dssdata)<-NULL
  dss.fit<-DSS.DE(dssdata,design[,2])
  ## dss seems to reverse the contrasts relative to other packages
  ## so have to flip the positive and negative values
  dss<-data.frame(miRNA=rownames(dss.fit),dss= - dss.fit$lfc)
  t18<-Sys.time()
  ######################################################################################
  ## EBseq now
  t19<-Sys.time()
  Sizes<-MedianNorm(datRatmat)
  EBSeqout<-EBTest(Data=datRatmat,Conditions=as.factor(design[,2]),
                   sizeFactors=Sizes,maxround=10)
  EBSeq.FC<-PostFC(EBSeqout)
  ## have to reverse signs as the fold change is calculated in the opposite direction
  ## than edgeR/deSEQ etc
  EBSeq.fit<-data.frame(miRNA=names(EBSeq.FC[[1]]),EBSeqPost=-log(EBSeq.FC[[1]],10))
  EBSeq.fit2<-data.frame(miRNA=names(EBSeq.FC[[2]]),EBSeqReal=-log(EBSeq.FC[[2]],10))
  t20<-Sys.time()
  ######################################################################################
  
  ## put the p-values and expression ratios together
  
  ## start with DESeq2
  
  res2<-res;res2$miRNA<-row.names(res2)
  res2<-as.data.frame(res2);res2<-res2[,c("miRNA","log2FoldChange")]
  names(res2)[2]<-"DESeq2"
  
  res2<-merge(res2,gamlssfit)
  res2<-merge(res2,gamlssfit2)
  res2<-merge(res2,edgeRfit)
  res2<-merge(res2,limtrfit)
  res2<-merge(res2,limtrfit.TMM)
  res2<-merge(res2,vmfit)
  res2<-merge(res2,vmfit.TMM)
  res2<-merge(res2,vmfit.SW)
  res2<-merge(res2,dss)
  res2<-merge(res2,EBSeq.fit)
  res2<-merge(res2,EBSeq.fit2)
  res2<-transform(res2,gamlss=log(exp(gamlss),10),
                  gamlssAD=log(exp(gamlssAD),10),
                  DESeq2=log(2^(DESeq2),10),
                  edgeR=log(2^(edgeR),10),
                  limmaTMM=log(2^(limmaTMM),10),
                  limma=log(2^(limma),10),
                  voom=log(2^(voom),10),
                  voomTMM=log(2^(voomTMM),10),
                  voomSW=log(2^(voomSW),10),
                  dss=log(exp(dss),10))

  ## obtain p-values and store them 
  gamlssADP<-summary(rep,"report",p.value=TRUE)[1:nlevels(datRat0$miRNA), 4]
  gamlssADP<-data.frame(miRNA=levels(datRat0$miRNA), gamlssADP=gamlssADP)
  
  edgeRP<-glmQLFTest(EdgeR)$table$PValue
  limtr.TMMP<-limtr.TMM$p.value[,2]
  limtrP<-limtr$p.value[,2]
  vm.TMMP<-vm.TMM$p.value[,2]
  vmP<-vm$p.value[,2]
  vm.SWP<-vm.SW$p.value[,2]
  DeSeq2P<-as.data.frame(res)$pvalue
  dssP<-dss.fit$pval
  EBSeqP<-EBSeqout$PPMat[,1]
  
  resP<-data.frame(gamlssADP, dssP,DeSeq2P,edgeRP,limtr.TMMP,limtrP,vmP,vm.TMMP,vm.SWP,EBSeqP)
  ## now do some cleanup
  rm(datRat0)
  ADtime=data.frame(time=c(t01-t00,t2-t1,t4-t3,t6-t5,t8-t7,t10-t9,t12-t11,
                           t14-t13,t16-t15,t18-t17,t20-t19),
                           method=c("gamlss","gamlssAD","DESeq2","edgeR","limmaTMM","limma",
                                    "voomTMM","voom","voomSW","dss","EBseq"))
  list(resCoef=res2,resP=resP,ADtime=ADtime)
}



dat<-subset(dat286.long,(Series =="Equi")) ## only the equimolar datasets
## get rid of the series column - we will append a new one
dat<-dat[,-6]




## stratified sampling

df<-unique(subset(dat,select=c(Amount,SampleID)))
row.names(df)<-NULL
set.seed(1234)
sam<-function(x)
{
  d<-stratified(x,"Amount",size=4,bothSets = T)
  d[[1]]$Series<-"A"
  d[[2]]$Series<-"B"
  d<-rbind(d[[1]],d[[2]])
  d
}

ransam<-replicate(200,sam(df),simplify=F)

## now we are ready to analyze the four equimolar datasets
## it will take time to do so - so the approach we would follow is 
## to run once and plot/summarize with an analysis script

t1<-Sys.time()
datRes<-lapply(ransam, function(x,d) try(fitNoDelta(x,d,"All"),TRUE),dat)
t2<-Sys.time()

print(t2-t1)

save(datRes,file="FDROutput_TMB.RData")



