## script that generates the processesed XLSX files deposited in GEO
## into 
library("xlsx") ## for reading XLSX files

## some info for the ratiometric series



proc286<-function(dat286)
{
  grpdilut<-data.frame(Series=c(rep("Equi",4),rep("RatioA",4),rep("RatioB",4)),
                       Subpool=rep(c("A","B","C","D"),3),
                       Dilution=c(rep("1:1",4),c("1:1","1:10","1:100","1:1000"),
                                  rev(c("1:1","1:10","1:100","1:1000")) ))
  expdet<-dat286[1:2,-(1:3)]
  nm<-colnames(expdet)
  nm<-gsub(".","",nm,fixed=TRUE)
  nm<-gsub("[[:digit:]]+$","",nm) ## this gives the group description of the experiments
  nm<-gsub("Equimolar","Equi",nm)
  expdet<-as.data.frame(t(expdet))
  names(expdet)<-c("Amount","SampleID")
  expdet$Series<-nm
  expdet<-merge(expdet,grpdilut)
  colnames(dat286)<-dat286[2,]
  dat286<-dat286[-(1:2),]
  
  
  names(dat286)[1:3]<-c("Sequence","miRNA","Subpool")
  dat286.long<-reshape(dat286,direction="long",varying=list(4:51),
                       v.names="reads",timevar="SampleID",
                       times=names(dat286)[4:51],idvar="miRNA")
  dat286.long<-merge(dat286.long,expdet)
  dat286.long$Pool<-"286"
  dat286.long$reads<-as.numeric(dat286.long$reads)
  dat286.long$Subpool<-factor(dat286.long$Subpool)
  dat286.long$miRNA<-factor(dat286.long$miRNA)
  dat286.long$Series<-factor(dat286.long$Series)
  dat286.long$SampleID<-factor(dat286.long$SampleID)
  dat286.long
}

dat286<-read.xlsx("286_libraries.xlsx",sheetIndex = 1,stringsAsFactors=FALSE)
dat286v2<-read.xlsx("286_libraries.xlsx",sheetIndex = 2,stringsAsFactors=FALSE)

dat286.long<-proc286(dat286)
save(dat286.long,file="286.RData") ## first version
dat286.long<-proc286(dat286v2)
save(dat286.long,file="286v2.RData") ## second version

## now to do the same for the miRXplore universe

procmiRX<-function(mirX)
{
  names(miRX)[1:2]<-c("miRNA","Sequence")
  miRX.long<-reshape(miRX,direction="long",varying=list(3:12),
                   v.names="reads",timevar="SampleID",
                   times=names(miRX)[3:12],idvar="miRNA")
  miRX.long$Amount<-"10 fmoles"
  miRX.long$Series<-"Equi"
  miRX.long$Pool<-"miRX"
  miRX.long$Dilution<-"1:1"
  miRX.long
}
miRX<-read.xlsx("mirxplore_libraries.xlsx",sheetIndex = 1,stringsAsFactors=FALSE)
miRXv2<-read.xlsx("mirxplore_libraries.xlsx",sheetIndex = 2,stringsAsFactors=FALSE)
miRX.long<-procmiRX(miRX)
save(miRX.long,file="miRX.RData")
miRX.long<-procmiRX(miRXv2)
save(miRX.long,file="miRXv2.RData")


## load legacy metadata
metadata<-read.xlsx("GEO_metadata.xlsx",sheetIndex=1,rowIndex=c(23:171))
## the first 32 are the legacy datasets
legmeta<-metadata[1:32,]
## load the actual data from the excel spreadsheet
dat<-read.xlsx("mirxplore_libraries.xlsx",sheetIndex = 3,stringsAsFactors=FALSE)
names(dat)<-gsub(".","-",names(dat),fixed=TRUE) ## fix periods to minus signs

legmeta<-subset(legmeta,select=c(Sample.name,characteristics..input.amount,
                                 characteristics..percent.PEG.in.ligation,
                                 characteristics..5..adapter.version))
names(legmeta)<-c("SampleID","Amount","PEG","X5padapter")
datGalas<-reshape(dat,direction="long","long",varying=list(3:34),
                  v.names="reads",timevar="SampleID",
                  times=names(dat)[3:34],idvar="Name")
names(datGalas)[1:2]<-c("miRNA","Sequence")
datGalas<-merge(datGalas,legmeta)
save(datGalas,file="legacyGalas.RData")
