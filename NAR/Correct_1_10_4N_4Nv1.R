## analyze the data via linear quadratic family
## generate correction factors for the analyses

library(gamlss)
library(mgcv)
source("LQNO.R")
rm(list=ls()) ## start with a clean slate

load("legacyGalas.RData")
dat<-datGalas
dat$miRNA<-factor(dat$miRNA)
dat$Amount<-factor(dat$Amount)
levels(dat$Amount)<-c("0.1 fmole","1 fmole","10 fmoles")

## random effects

datLow<-subset(dat,Amount=="1 fmole")
datLow$SampleID<-factor(datLow$SampleID)
datHigh<-subset(dat,Amount=="10 fmoles")
datHigh$SampleID<-factor(datHigh$SampleID)

## 



## fit the  models to the datasets
f.low.r<-gamlss(reads~ SampleID+random(miRNA),sigma.fo=~ SampleID+random(miRNA),data=datLow,family="NBI",
                control=gamlss.control(n.cyc=5000,c.crit=0.001,gd.tol=Inf),method=RS())
f.high.r<-gamlss(reads~ SampleID+random(miRNA),sigma.fo=~ SampleID+random(miRNA),data=datHigh,family="NBI",
                control=gamlss.control(n.cyc=5000,c.crit=0.001,gd.tol=Inf),method=RS())


## we need to fit 2 correction scenarios L->H, H->L

## first the prediction values for the Low
pred<-predictAll(f.low.r,type="terms",data=datLow)
datLow$off.m<-pred$mu[,2]
datLow$off.s<-pred$sigma[,2]
datLow$SampleID<-factor(datLow$SampleID)
offLow<-subset(datLow,select=c(miRNA,off.m,off.s),SampleID==levels(datLow$SampleID)[1])
names(offLow)[2:3]<-paste("low",names(offLow)[2:3],sep=".")

## predictions for the high
pred<-predictAll(f.high.r,type="terms")
datHigh$off.m<-pred$mu[,2]
datHigh$off.s<-pred$sigma[,2]
datHigh$SampleID<-factor(datHigh$SampleID)
offHigh<-subset(datHigh,select=c(miRNA,off.m,off.s),SampleID==levels(datHigh$SampleID)[1])
names(offHigh)[2:3]<-paste("High",names(offHigh)[2:3],sep=".")

off<-merge(offLow,offHigh)


## now to use these variables for offset analysis

datLow0<-merge(off,datLow)
datHigh0<-merge(datHigh,off)


## now fit the models - naming convention f.{development dataset}.{validation dataset}
f.low.high<-gamlss(reads~ SampleID+random(miRNA)+offset(low.off.m),
                        sigma.fo=~ SampleID+random(miRNA)+offset(low.off.s),data=datHigh0,family="NBI",
                        control=gamlss.control(n.cyc=5000,c.crit=0.01),method=RS())

f.high.low<-gamlss(reads~ SampleID+random(miRNA)+offset(High.off.m),
                        sigma.fo=~ SampleID+random(miRNA)+offset(High.off.s),data=datLow0,family="NBI",
                        control=gamlss.control(n.cyc=5000,c.crit=0.01,sigma.step=0.01),method=RS())


save(off,datLow0,datHigh0,
     datLow,datHigh,off,f.high.low,f.low.high,
     f.low.r,f.high.r,
     file="Galas_Correction_Legacy.RData")
