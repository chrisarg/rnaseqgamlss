## random factors to assess differential expression changes in GAMLSS
library(gamlss)
library(gamlss.mx)
library(DESeq2)
library(rnaseqGene)
library(mgcv)
library(ks)
library(ggplot2)
library(mclust) ## for mixture model KDE
library(edgeR)
library(plyr)
library(limma)
library(DSS)
library(EBSeq)
library(TMB)
rm(list=ls()) ## start with a clean slate
source("LQNO.R")
load("286v2.RData") ## load the v2, to generate the v2 image files
dyn.load(dynlib("LQNO_DE"))
dyn.load(dynlib("NBI_DE"))

######################################################################################
## Prepare data for GAMLSS
datRat<-subset(dat286.long,(Series=="Equi") & (Amount=="0.1 fmole" | Amount=="100 fmoles"))
datRat$SampleID<-factor(datRat$SampleID)
datRat$Amount<-factor(datRat$Amount)

## Prepare data for the DeSEQ2 and the other programs that expect a sequence count
## and a design matrix as input. For each program specific data structures are
## created prior to calling the fitting functions
datRatmat<-reshape(subset(datRat,select=c(miRNA,reads,SampleID)),direction="wide",
                   v.names="reads",timevar="SampleID",idvar="miRNA")
rownames(datRatmat)<-datRatmat[,1]
datRatmat<-as.matrix(datRatmat[,-1])
colData<-unique(subset(datRat,select=c(SampleID,Amount,Series)))
colData$SampleID<-paste("reads",as.character(colData$SampleID),sep=".")
rownames(colData)<-colData[,1]
colData<-colData[,-1]
design<-model.matrix(~colData$Amount) ## design matrix


######################################################################################
## GAMLSS APPROACH


## fit random effect model to find standard deviation BLUPS
t1<-Sys.time()
fit1<-gamlss(reads~ Amount+re(random=list(miRNA=pdIdent(~Amount)),method="REML",
                              returnObject=TRUE,maxIter=5000,msMaxIter=5000,
                              niterEM=150,apVar=FALSE),
             sigma.fo=~ Amount+re(random=list(miRNA=pdIdent(~Amount)),method="REML",
                                  returnObject=TRUE,maxIter=5000,msMaxIter=5000,
                                  niterEM=150,apVar=FALSE),
             data=datRat,family="NBI",
             control=gamlss.control(n.cyc=5000,c.crit=0.001,trace=TRUE),method=RS())
t2<-Sys.time()

print(t2-t1)
gamlssfit<-ranef(getSmo(fit1))
gamlssfit$miRNA<-row.names(gamlssfit);gamGAMLSS<-gamlssfit
gamlssfit<-gamlssfit[,c(3,2)]
names(gamlssfit)[2]<-"gamlss";
gamlssfit<-transform(gamlssfit,gamlss=gamlss+fit1$mu.coefficients[2])


## now fit the TMB model for the p-values. 
## Fix the standard deviation to their values

u_X<-as.numeric(factor(datRat$miRNA))
u_G<-as.numeric(factor(datRat$Amount))
y=datRat$reads
X<-model.matrix(~Amount,data=datRat)
sigmu=rep(1,max(u_G))
sigsig=rep(1,max(u_G))
b=rep(0,ncol(X));
s_b=rep(0,ncol(X));
u_m=matrix(0,max(u_X),max(u_G));
u_s=matrix(0,max(u_X),max(u_G))
obj.TMB<-MakeADFun(data=list(y=y,X=X,u_X=u_X,u_G=u_G),
                   parameters=list(b=b,s_b=s_b,u_m=u_m,u_s=u_s,
                                   sigmu=sigmu,sigsig=sigsig),
                   DLL="NBI_DE",random=c("u_m","u_s"),hessian=TRUE,silent=TRUE,
                   method="BFGS",random.start=expression(last.par[random]),
                   ADReport=TRUE)
t3<-Sys.time()
f.TMB<-optim(obj.TMB$par,obj.TMB$fn,obj.TMB$gr,method="L-BFGS",
              control=list(maxit=100000),lower=-30,upper=30)
t4<-Sys.time()
print(t4-t3)
t5<-Sys.time()
rep<-sdreport(obj.TMB)
t6<-Sys.time()
print(t6-t5)
## transform the coefficient and the covarience matrix to obtain differential
## expression levels in the second group. 
dummy<-summary(rep,"report")[1:nlevels(datRat$miRNA),"Estimate"]

gamlssfit2<-data.frame(miRNA=levels(datRat$miRNA),gamlssAD=dummy)
######################################################################################
## now use DESeq2
dds <- DESeqDataSetFromMatrix(countData = datRatmat, colData = colData, design = ~ Amount) 
dds <- DESeq(dds) 
res <- results(dds)
######################################################################################
## now use EdgeR
EdgeRdata<-DGEList(datRatmat,group=as.numeric(colData$Series))
EdgeRdata.TMM <- calcNormFactors(EdgeRdata)
EdgeRdata.TMM <- estimateDisp(EdgeRdata.TMM, design,trend.method = "loess",robust=TRUE)
EdgeR<-glmQLFit(EdgeRdata.TMM,design=design)
edgeRfit<-data.frame(miRNA=row.names(EdgeR$coefficients),edgeR=EdgeR$coefficients[,2])
######################################################################################
## limma-trend with TMM
EdgeRdata.TMM <- calcNormFactors(EdgeRdata)
logCPM.TMM<-cpm(EdgeRdata.TMM,log=TRUE,prior.count=3)
limtr.TMM<-lmFit(logCPM.TMM,design=design)
limtr.TMM <- eBayes(limtr.TMM, trend=TRUE)
limtrfit.TMM<-as.data.frame(treat(limtr.TMM)$coef)
limtrfit.TMM$miRNA<-rownames(limtrfit.TMM)
limtrfit.TMM<-limtrfit.TMM[,c(3,2)]
names(limtrfit.TMM)[2]<-"limmaTMM"
######################################################################################
## limma-trend without TMM
logCPM<-cpm(EdgeRdata,log=TRUE,prior.count=3)
limtr<-lmFit(logCPM,design=design)
limtr <- eBayes(limtr, trend=TRUE)
limtrfit<-as.data.frame(treat(limtr)$coef)
limtrfit$miRNA<-rownames(limtrfit)
limtrfit<-limtrfit[,c(3,2)]
names(limtrfit)[2]<-"limma"
######################################################################################
## voom (applies TMM)
voomData.TMM<-voom(EdgeRdata.TMM,design,plot=FALSE)
vm.TMM<-lmFit(voomData.TMM,design=design)
vm.TMM <- eBayes(vm.TMM, trend=TRUE)
vmfit.TMM<-as.data.frame(treat(vm.TMM)$coef)
vmfit.TMM$miRNA<-rownames(vmfit.TMM)
vmfit.TMM<-vmfit.TMM[,c(3,2)]
names(vmfit.TMM)[2]<-"voomTMM"
######################################################################################
## voom without normalization
voomData<-voom(EdgeRdata,design,plot=FALSE)
vm<-lmFit(voomData,design=design)
vm <- eBayes(vm, trend=TRUE)
vmfit<-as.data.frame(treat(vm)$coef)
vmfit$miRNA<-rownames(vmfit)
vmfit<-vmfit[,c(3,2)]
names(vmfit)[2]<-"voom"
######################################################################################
## voom without normalization but with sample weights
voomData.SW<-voomWithQualityWeights(EdgeRdata,design,plot=FALSE,norm="none")
vm.SW<-lmFit(voomData.SW,design=design)
vm.SW <- eBayes(vm.SW, trend=TRUE)
vmfit.SW<-as.data.frame(treat(vm.SW)$coef)
vmfit.SW$miRNA<-rownames(vmfit.SW)
vmfit.SW<-vmfit.SW[,c(3,2)]
names(vmfit.SW)[2]<-"voomSW"
######################################################################################
## DSS now
dssdata<-datRatmat;colnames(dssdata)<-NULL
dss.fit<-DSS.DE(dssdata,design[,2])
## dss seems to reverse the contrasts relative to other packages
## so have to flip the positive and negative values
dss<-data.frame(miRNA=rownames(dss.fit),dss= - dss.fit$lfc)
######################################################################################
## EBseq now
Sizes<-MedianNorm(datRatmat)
EBSeqout<-EBTest(Data=datRatmat,Conditions=as.factor(design[,2]),
                  sizeFactors=Sizes,maxround=10)
EBSeq.FC<-PostFC(EBSeqout)
## have to reverse signs as the fold change is calculated in the opposite direction
## than edgeR/deSEQ etc
EBSeq.fit<-data.frame(miRNA=names(EBSeq.FC[[1]]),EBSeqPost=-log(EBSeq.FC[[1]],10))
EBSeq.fit2<-data.frame(miRNA=names(EBSeq.FC[[2]]),EBSeqReal=-log(EBSeq.FC[[2]],10))
######################################################################################
## put everything together now - centering GAMLSS estimates
## start with DESeq2

res2<-res;res2$miRNA<-row.names(res2)
res2<-as.data.frame(res2);res2<-res2[,c("miRNA","log2FoldChange")]
names(res2)[2]<-"DESeq2"

res2<-merge(res2,gamlssfit)
res2<-merge(res2,gamlssfit2)
res2<-merge(res2,edgeRfit)
res2<-merge(res2,limtrfit)
res2<-merge(res2,limtrfit.TMM)
res2<-merge(res2,vmfit)
res2<-merge(res2,vmfit.TMM)
res2<-merge(res2,vmfit.SW)
res2<-merge(res2,dss)
res2<-merge(res2,EBSeq.fit)
res2<-merge(res2,EBSeq.fit2)
res2<-transform(res2,gamlss=log(exp(gamlss),10),
                gamlssAD=log(exp(gamlssAD),10),
                DESeq2=log(2^(DESeq2),10),
                edgeR=log(2^(edgeR),10),
                limmaTMM=log(2^(limmaTMM),10),
                limma=log(2^(limma),10),
                voom=log(2^(voom),10),
                voomTMM=log(2^(voomTMM),10),
                voomSW=log(2^(voomSW),10),
                dss=log(exp(dss),10))
######################################################################################
## intermediate calculations for plotting
datDens<-as.list(res2[,-1])

d0<-unlist(datDens)
newdata<-seq(-1,2,.01)
fitDens<-lapply(datDens,function(x) densityMclust(x,G=1:10,modelName="V"))
prDens<-lapply(fitDens,function(x,dt) dens(x$modelName,data=dt,parameters=x$parameters),newdata)
dfprDens<-as.data.frame(prDens)
dfprDens$x<-newdata

## xintercepts of true values based on PCR efficiencies from 0.80 to 0.99
itc<-3-7*c(log(1+0.8,10),log(1+0.99,10)) 
dfprDens<-reshape(dfprDens,direction="long",varying=list(1:12),timevar = "method",
                    idvar="x",times=colnames(dfprDens)[1:12],v.names="meas")

######################################################################################
## plot

ggplot(dfprDens,aes(x=x,y=meas,facets=method))+geom_line()+theme_bw()+
  facet_wrap(~method)+geom_vline(xintercept=0,col="red",linetype="dashed")+
  xlab("Fold Change (log10)")+ylab("Density")+scale_x_continuous(breaks=seq(-1,1.5,1))+
  scale_y_continuous(breaks=seq(0,3,1))+  
  theme(strip.text.x = element_text(size=18),axis.title=element_text(size=18))+
  theme(axis.text=element_text(size=14))
ggsave("RandomInterceptSlope_286v2_Equi_TMB.png",width=8,heigh=8,units="in",dpi=1200)



## now do the p values
## obtain p-values and store them 
gamlssADP<-summary(rep,"report",p.value=TRUE)[1:nlevels(datRat$miRNA), 4]
gamlssADP<-data.frame(miRNA=levels(datRat$miRNA), gamlssADP=gamlssADP)

edgeRP<-data.frame(miRNA=row.names(EdgeR$coefficients),edgeRP=glmQLFTest(EdgeR)$table$PValue)
limtr.TMMP<-data.frame(miRNA=limtrfit.TMM$miRNA,limtr.TMPP=limtr.TMM$p.value[,2])
limtrP<-data.frame(miRNA=limtrfit$miRNA,limtrP=limtr$p.value[,2])
vm.TMMP<-data.frame(miRNA=vmfit.TMM$miRNA,vm.TMMP=vm.TMM$p.value[,2])
vmP<-data.frame(miRNA=vmfit$miRNA,vmP=vm$p.value[,2])
vm.SWP<-data.frame(miRNA=vmfit.SW$miRNA,vm.SWP=vm.SW$p.value[,2])
DeSeq2P<-data.frame(miRNA=res2$miRNA,DeSeq2P=as.data.frame(res)$pvalue)
dssP<-data.frame(miRNA=rownames(dss.fit),dssP=dss.fit$pval)
EBSeqP<-data.frame(miRNA=names(EBSeq.FC[[1]]),EBSeqP=EBSeqout$PPMat[,1])

resP<-DeSeq2P
resP<-merge(resP,gamlssADP)
resP<-merge(resP,edgeRP)
resP<-merge(resP,limtrP)
resP<-merge(resP,limtr.TMMP)
resP<-merge(resP,vmP)
resP<-merge(resP,vm.TMMP)
resP<-merge(resP,vm.SWP)
resP<-merge(resP,dssP)
resP<-merge(resP,EBSeqP)


resP.long<-reshape(resP,direction="long",varying=list(2:11),timevar = "method",
                   idvar="miRNA",times=colnames(resP)[2:11],v.names="p")

resP.long$Dataset<-"TwoEquiv2"
write.csv(resP.long,file="pvalue_TwoEquiv2.csv",row.names=F)

sink("RandomInterceptSlope_286v2_Equi_Pvalues_TMB.txt")
cat("Proportion of miRNAs with p values less than 0.05\n")
apply(resP[,-c(1:1)],2,function(x) sum(x<=0.05)/length(x))
sink()

