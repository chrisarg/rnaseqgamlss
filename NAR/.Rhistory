library(gamlss.mx)
library(DESeq2)
library(rnaseqGene)
library(mgcv)
library(ks)
library(ggplot2)
library(mclust) ## for mixture model KDE
library(edgeR)
library(plyr)
library(limma)
library(DSS)
library(EBSeq)
library(TMB)
rm(list=ls()) ## start with a clean slate
source("LQNO.R")
load("286v2.RData") ## load the v2, to generate the v2 image files
dyn.load(dynlib("LQNO_DE"))
dyn.load(dynlib("NBI_DE"))
######################################################################################
## Prepare data for GAMLSS
datRat<-subset(dat286.long,(Series=="RatioB" | Series =="RatioA") &
Amount=="100 fmoles" &(Subpool =="A" | Subpool=="B"))
datRat$SampleID<-factor(datRat$SampleID)
datRat$Series<-factor(datRat$Series)
datRat$miRNA<-factor(datRat$miRNA)
## Prepare data for the DeSEQ2 and the other programs that expect a sequence count
## and a design matrix as input. For each program specific data structures are
## created prior to calling the fitting functions
datRatmat<-reshape(subset(datRat,select=c(miRNA,reads,SampleID)),direction="wide",
v.names="reads",timevar="SampleID",idvar="miRNA")
rownames(datRatmat)<-datRatmat[,1]
datRatmat<-as.matrix(datRatmat[,-1])
colData<-unique(subset(datRat,select=c(SampleID,Amount,Series)))
colData$SampleID<-paste("reads",as.character(colData$SampleID),sep=".")
rownames(colData)<-colData[,1]
colData<-colData[,-1]
design<-model.matrix(~colData$Series) ## design matrix
######################################################################################
## GAMLSS APPROACH
## fit random effect model to find standard deviation BLUPS
t1<-Sys.time()
fit1<-gamlss(reads~ Series+re(random=list(miRNA=pdIdent(~Series)),method="REML",
returnObject=TRUE,maxIter=5000,msMaxIter=5000,
niterEM=150,apVar=FALSE),
sigma.fo=~ Series+re(random=list(miRNA=pdIdent(~Series)),method="REML",
returnObject=TRUE,maxIter=5000,msMaxIter=5000,
niterEM=150,apVar=FALSE),
data=datRat,family="NBI",
control=gamlss.control(n.cyc=5000,c.crit=0.001,trace=TRUE),method=RS())
t2<-Sys.time()
print(t2-t1)
gamlssfit<-ranef(getSmo(fit1))
gamlssfit$miRNA<-row.names(gamlssfit)
gamlssfit<-gamlssfit[,c(3,2)]
names(gamlssfit)[2]<-"gamlss"
gamlssfit<-transform(gamlssfit,gamlss=gamlss+fit1$mu.coefficients[2])
## now fit the fixed effect model for the p-values.
u_X<-as.numeric(factor(datRat$miRNA))
u_G<-as.numeric(factor(datRat$Series))
y=datRat$reads
X<-model.matrix(~Series,data=datRat)
sigmu=rep(1,max(u_G))
sigsig=rep(1,max(u_G))
b=rep(0,ncol(X))
s_b=rep(0,ncol(X))
u_m=matrix(0,max(u_X),max(u_G))
u_s=matrix(0,max(u_X),max(u_G))
obj.TMB<-MakeADFun(data=list(y=y,X=X,u_X=u_X,u_G=u_G),
parameters=list(b=b,s_b=s_b,u_m=u_m,u_s=u_s,
sigmu=sigmu,sigsig=sigsig),
DLL="NBI_DE",random=c("u_m","u_s"),hessian=TRUE,silent=TRUE,
method="BFGS",random.start=expression(last.par[random]),
ADReport=TRUE)
t3<-Sys.time()
f.TMB<-nlminb(obj.TMB$par,obj.TMB$fn,obj.TMB$gr,
control=list(eval.max=10000,iter.max=10000),lower=-30,upper=30)
t4<-Sys.time()
print(t4-t3)
t5<-Sys.time()
rep<-sdreport(obj.TMB)
t6<-Sys.time()
print(t6-t5)
## transform the coefficient and the covarience matrix to obtain differential
## expression levels in the second group.
dummy<-summary(rep,"report")[1:nlevels(datRat$miRNA),"Estimate"]
gamlssfit2<-data.frame(miRNA=levels(datRat$miRNA),gamlssAD=dummy)
######################################################################################
## now use DESeq2
dds <- DESeqDataSetFromMatrix(countData = datRatmat, colData = colData, design = ~ Series)
dds <- DESeq(dds)
res <- results(dds)
######################################################################################
## now use EdgeR
EdgeRdata<-DGEList(datRatmat,group=as.numeric(colData$Series))
EdgeRdata.TMM <- calcNormFactors(EdgeRdata)
EdgeRdata.TMM <- estimateDisp(EdgeRdata.TMM, design,trend.method = "loess",robust=TRUE)
EdgeR<-glmQLFit(EdgeRdata.TMM,design=design)
edgeRfit<-data.frame(miRNA=row.names(EdgeR$coefficients),edgeR=EdgeR$coefficients[,2])
######################################################################################
## limma-trend with TMM
EdgeRdata.TMM <- calcNormFactors(EdgeRdata)
logCPM.TMM<-cpm(EdgeRdata.TMM,log=TRUE,prior.count=3)
limtr.TMM<-lmFit(logCPM.TMM,design=design)
limtr.TMM <- eBayes(limtr.TMM, trend=TRUE)
limtrfit.TMM<-as.data.frame(treat(limtr.TMM)$coef)
limtrfit.TMM$miRNA<-rownames(limtrfit.TMM)
limtrfit.TMM<-limtrfit.TMM[,c(3,2)]
names(limtrfit.TMM)[2]<-"limmaTMM"
######################################################################################
## limma-trend without TMM
logCPM<-cpm(EdgeRdata,log=TRUE,prior.count=3)
limtr<-lmFit(logCPM,design=design)
limtr <- eBayes(limtr, trend=TRUE)
limtrfit<-as.data.frame(treat(limtr)$coef)
limtrfit$miRNA<-rownames(limtrfit)
limtrfit<-limtrfit[,c(3,2)]
names(limtrfit)[2]<-"limma"
######################################################################################
## voom (applies TMM)
voomData.TMM<-voom(EdgeRdata.TMM,design,plot=FALSE)
vm.TMM<-lmFit(voomData.TMM,design=design)
vm.TMM <- eBayes(vm.TMM, trend=TRUE)
vmfit.TMM<-as.data.frame(treat(vm.TMM)$coef)
vmfit.TMM$miRNA<-rownames(vmfit.TMM)
vmfit.TMM<-vmfit.TMM[,c(3,2)]
names(vmfit.TMM)[2]<-"voomTMM"
######################################################################################
## voom without normalization
voomData<-voom(EdgeRdata,design,plot=FALSE)
vm<-lmFit(voomData,design=design)
vm <- eBayes(vm, trend=TRUE)
vmfit<-as.data.frame(treat(vm)$coef)
vmfit$miRNA<-rownames(vmfit)
vmfit<-vmfit[,c(3,2)]
names(vmfit)[2]<-"voom"
######################################################################################
## voom without normalization but with sample weights
voomData.SW<-voomWithQualityWeights(EdgeRdata,design,plot=FALSE,norm="none")
vm.SW<-lmFit(voomData.SW,design=design)
vm.SW <- eBayes(vm.SW, trend=TRUE)
vmfit.SW<-as.data.frame(treat(vm.SW)$coef)
vmfit.SW$miRNA<-rownames(vmfit.SW)
vmfit.SW<-vmfit.SW[,c(3,2)]
names(vmfit.SW)[2]<-"voomSW"
######################################################################################
## DSS now
dssdata<-datRatmat;colnames(dssdata)<-NULL
dss.fit<-DSS.DE(dssdata,design[,2])
## dss seems to reverse the contrasts relative to other packages
## so have to flip the positive and negative values
dss<-data.frame(miRNA=rownames(dss.fit),dss= - dss.fit$lfc)
######################################################################################
## EBseq now
Sizes<-MedianNorm(datRatmat)
EBSeqout<-EBTest(Data=datRatmat,Conditions=as.factor(design[,2]),
sizeFactors=Sizes,maxround=10)
EBSeq.FC<-PostFC(EBSeqout)
## have to reverse signs as the fold change is calculated in the opposite direction
## than edgeR/deSEQ etc
EBSeq.fit<-data.frame(miRNA=names(EBSeq.FC[[1]]),EBSeqPost=-log(EBSeq.FC[[1]],10))
EBSeq.fit2<-data.frame(miRNA=names(EBSeq.FC[[2]]),EBSeqReal=-log(EBSeq.FC[[2]],10))
######################################################################################
## put everything together now
## start with DESeq2
res2<-res;res2$miRNA<-row.names(res2)
res2<-as.data.frame(res2);res2<-res2[,c("miRNA","log2FoldChange")]
names(res2)[2]<-"DESeq2"
res2<-merge(res2,gamlssfit)
res2<-merge(res2,gamlssfit2)
res2<-merge(res2,edgeRfit)
res2<-merge(res2,limtrfit)
res2<-merge(res2,limtrfit.TMM)
res2<-merge(res2,vmfit)
res2<-merge(res2,vmfit.TMM)
res2<-merge(res2,vmfit.SW)
res2<-merge(res2,dss)
res2<-merge(res2,EBSeq.fit)
res2<-merge(res2,EBSeq.fit2)
res2<-transform(res2,gamlss=log(exp(gamlss),10),
gamlssAD=log(exp(gamlssAD),10),
DESeq2=log(2^(DESeq2),10),
edgeR=log(2^(edgeR),10),
limmaTMM=log(2^(limmaTMM),10),
limma=log(2^(limma),10),
voom=log(2^(voom),10),
voomTMM=log(2^(voomTMM),10),
voomSW=log(2^(voomSW),10),
dss=log(exp(dss),10))
######################################################################################
## intermediate calculations for plotting
datDens<-as.list(res2[,-1])
d0<-unlist(datDens)
newdata<-seq(-4,2,.01)
fitDens<-lapply(datDens,function(x) densityMclust(x,G=1:10,modelName="V"))
prDens<-lapply(fitDens,function(x,dt) dens(x$modelName,data=dt,parameters=x$parameters),newdata)
dfprDens<-as.data.frame(prDens)
dfprDens$x<-newdata
## calculation of true values for the expression change:
## the ascending  series is               1:1   1:10   1:100  1:1000
## the descending series is             1:1000  1:100   1:10    1:1
## in log10 space the true values are    -3      -1      1       3
itc<-c(-3,-1) ## xintercepts of true values
dfprDens<-reshape(dfprDens,direction="long",varying=list(1:12),timevar = "method",
idvar="x",times=colnames(dfprDens)[1:12],v.names="meas")
## RMS calculations
truth<-unique(subset(datRat,select=c(miRNA,Dilution),Series=="RatioB"))
tr<-data.frame(Dilution=unique(truth$Dilution),trVal=(itc))
truth<-merge(truth,tr)
RMSdf<-merge(res2,truth[,2:3])
## reshape to long format to calculate summaries per group
RMSdf.long<-reshape(RMSdf,direction="long",varying=list(2:13),timevar = "method",
idvar="miRNA",times=colnames(RMSdf)[2:13],v.names="meas")
## RMS calculations per group
rms<-ddply(RMSdf.long,.(trVal,method),.fun=function(x) sqrt(mean((x$meas-x$trVal)^2)))
names(rms)[3]<-"RMS"
## now generate global RMS summaries
RMSdf[,2:(ncol(RMSdf)-1)]<-(RMSdf[,2:(ncol(RMSdf)-1)]-RMSdf[,ncol(RMSdf)])^2
sumdf<-data.frame(RMS=apply(RMSdf[,2:(ncol(RMSdf)-1)],2,function(x) RMS=sqrt(mean(x))))
sumdf$method<-row.names(sumdf)
sumdf$RMS<-round(sumdf$RMS,2)
rms$RMS<-round(rms$RMS,2)
######################################################################################
## plot
ggplot(dfprDens,aes(x=x,y=meas,facets=method))+geom_line()+theme_bw()+
facet_wrap(~method)+geom_vline(xintercept=itc,col="red",linetype="dashed")+
xlab("Fold Change (log10)")+ylab("Density")+scale_x_continuous(breaks=seq(-3,2,2))+
scale_y_continuous(breaks=seq(0,1.6,.4),limits=c(-0.1,1.6))+
geom_text(data=sumdf,aes(x=-2,y=-0.07,label=RMS,fontface="bold"),size=8)+
theme(strip.text.x = element_text(size=18),axis.title=element_text(size=18))+
theme(axis.text=element_text(size=14))
ggsave("RandomInterceptSlope_286v2_SeriesASeriesB_half_TMB.png",width=8,heigh=8,units="in",dpi=1200)
## now do the p values
## obtain p-values and store them
gamlssADP<-summary(rep,"report",p.value=TRUE)[1:nlevels(datRat$miRNA), 4]
gamlssADP<-data.frame(miRNA=levels(datRat$miRNA), gamlssADP=gamlssADP)
edgeRP<-data.frame(miRNA=row.names(EdgeR$coefficients),edgeRP=glmQLFTest(EdgeR)$table$PValue)
limtr.TMMP<-data.frame(miRNA=limtrfit.TMM$miRNA,limtr.TMPP=limtr.TMM$p.value[,2])
limtrP<-data.frame(miRNA=limtrfit$miRNA,limtrP=limtr$p.value[,2])
vm.TMMP<-data.frame(miRNA=vmfit.TMM$miRNA,vm.TMMP=vm.TMM$p.value[,2])
vmP<-data.frame(miRNA=vmfit$miRNA,vmP=vm$p.value[,2])
vm.SWP<-data.frame(miRNA=vmfit.SW$miRNA,vm.SWP=vm.SW$p.value[,2])
DeSeq2P<-data.frame(miRNA=res2$miRNA,DeSeq2P=as.data.frame(res)$pvalue)
dssP<-data.frame(miRNA=rownames(dss.fit),dssP=dss.fit$pval)
EBSeqP<-data.frame(miRNA=names(EBSeq.FC[[1]]),EBSeqP=EBSeqout$PPMat[,1])
resP<-DeSeq2P
resP<-merge(resP,gamlssADP)
resP<-merge(resP,edgeRP)
resP<-merge(resP,limtrP)
resP<-merge(resP,limtr.TMMP)
resP<-merge(resP,vmP)
resP<-merge(resP,vm.TMMP)
resP<-merge(resP,vm.SWP)
resP<-merge(resP,dssP)
resP<-merge(resP,EBSeqP)
resP<-merge(truth,resP)
resP.long<-reshape(resP,direction="long",varying=list(4:13),timevar = "method",
idvar="miRNA",times=colnames(resP)[4:13],v.names="p")
sink("RandomInterceptSlope_286v2_SeriesASeriesB_half_Pvalues_TMB.txt")
cat("Proportion of miRNAs with p values less than 0.05\n")
apply(resP[,-c(1:3)],2,function(x) sum(x<=0.05)/length(x))
cat("Proportion of miRNAs with p values less than 0.05, stratified by true value\n")
ddply(resP.long,.(method,trVal),summarize,TDR=sum(p<=0.05)/length(p))
sink()
## analyze type I, type 2, FDR and false omission rates from the DE scenarios
library(lme4)
library(boot)
library(ggplot2)
library(plyr)
library(grid)
rm(list=ls()) ## start with a clean slate
FDR<-function(prev,Sens,Spec) 1-Sens*prev/(Sens*prev+(1-Spec)*(1-prev))
FOR<-function(prev,Sens,Spec) 1-Spec*(1-prev)/((1-Sens)*prev+Spec*(1-prev))
funSum<-function(x){
y=mean(x)
se=sd(x)
ymin=quantile(x,0.025)
ymax=quantile(x,.975)
data.frame(y,ymin,ymax)
}
## now load sensitivity (= 1-type 2)
sensDat<-read.csv("Power_Methods.csv")
sensDat.long<-reshape(sensDat,direction="long",varying=list(2:11),timevar = "method",
idvar="id",times=colnames(sensDat)[2:11],v.names="p")
sensDat.long$method<-factor(sensDat.long$method)
## weights are the numbers of tests - logistic regression analyses to find differences
## between methods
weights=rep(286,nrow(sensDat.long))
sensDat.long$Tot=weights
sensDat.long$Suc=round(sensDat.long$p*sensDat.long$Tot)
sensDat.long<-transform(sensDat.long,Fail=Tot-Suc)
fSens<-glmer(cbind(Suc,Fail)~method+(1|Dataset),data=sensDat.long,family=binomial,nAGQ=0)
## now load type I errors
load("FDROutput_August_TMB.RData")
datRes<-datRes[sapply(datRes,class)=="list"]
## typeI
typeIfun<-function(x,p=0.05) {I(x<=p)}
typeIDat<-sapply(1:length(datRes),function(x) {df=datRes[[x]][[2]];
ret=as.data.frame(apply(df[,-1],2,typeIfun));ret$Dataset=x;ret},simplify=FALSE)
typeIDat<-do.call(rbind,typeIDat)
rownames(typeIDat)<-NULL
typeI.long<-reshape(typeIDat,direction="long",varying=list(1:10),timevar = "method",
idvar="id",times=colnames(typeIDat)[1:10],v.names="p")
typeI.long$method<-factor(typeI.long$method)
typeI.long$p<-as.numeric(typeI.long$p)
typeI.long<-na.omit(typeI.long)
typeI.long$Dataset<-factor(typeI.long$Dataset)
typeI.long<-ddply(typeI.long,.(Dataset,method),summarize,Suc=sum(p),Fail=length(p)-sum(p),Tot=length(p))
ftypeI<-glmer(cbind(Suc,Fail)~method+(1|Dataset),data=typeI.long,family=binomial,nAGQ=0)
## Save results of analyses - for use in the paper
sink("RegressionAnalysisOfTypeIType2.txt")
cat("Type I error (logistic regression)\n")
summary(ftypeI)
cat("\n-----------------------------------------------------------\n")
cat("Type II error (logistic regression)\n")
summary(fSens)
sink()
## now bootstrap to get estimates of type I errors and sensitivities
myEst<-function(.){
logits<-getME(.,"beta")
logits<-logits+c(0,rep(logits[1],length(logits)-1))
inv.logit(logits)
}
set.seed(1234) - ## bootstrap is computational exp
system.time(SensBoot<-bootMer(fSens,myEst,nsim=1000))
system.time(typeIBoot<-bootMer(ftypeI,myEst,nsim=1000))
#save(SensBoot,typeIBoot,file="BootlmerTypeIType2.RData")
## create more sensitive labels
labs=c("DESeq2","dss","EBSeq","edgeR","gamlssAD","limmaTMM",
"limma","voomSW","voomTMM","voom")
Sens<-SensBoot[[2]]
Sp<-1-typeIBoot[[2]]
colnames(Sens)<-levels(sensDat.long$method)
colnames(Sp)<-levels(sensDat.long$method)
prev<-seq(0.005,.50,.01)
fdrV<-sapply(prev,function(x) cbind(x,FDR(x,Sens,Sp)),simplify=FALSE)
fdrV<-as.data.frame(do.call(rbind,fdrV))
fdrV$metric="FDR"
forV<-sapply(prev,function(x) cbind(x,FOR(x,Sens,Sp)),simplify=FALSE)
forV<-as.data.frame(do.call(rbind,forV))
forV$metric="FOR"
DR<-rbind(fdrV,forV)
DR<-reshape(DR,direction="long",varying=list(c(2:11)),timevar="Method",idvar="id",
times=colnames(DR)[c(2:11)],v.names="meas")
DR$Method<-factor(DR$Method,labels=labs)
ggplot(DR,aes(x,meas,facets=metric,color=Method))+stat_summary(fun.data=funSum,geom="line")+
facet_wrap(~metric,scales="free",nrow=1)+xlab("% of DE RNAs")+ylab("Metric")+theme_bw()+
scale_x_continuous(breaks=c(0.0,.15,.30,.45))+
theme(strip.text.x = element_text(size=18),axis.title=element_text(size=18))+
theme(axis.text=element_text(size=14))+theme(legend.text=element_text(size=12))+
theme(legend.title=element_text(size=14))
ggsave("FDRFOR.png",width=15.575,height=12.075,units="cm",dpi=1200)
ggplot(DR,aes(x,meas))+stat_summary(fun.data=funSum,geom="smooth")+
facet_grid(metric~Method,scales="free")+xlab("% of DE RNAs")+ylab("Metric")+theme_bw()+
scale_x_continuous(breaks=c(.10,.30,.5))
ggsave("FDRFOR_CI.png",width=12,height=6,dpi=1200,units="in")
Sens.mn<-apply(Sens,2,mean)
Sp.mn<-apply(1-Sp,2,mean)
ROC<-data.frame(Sens=Sens.mn,Sp=Sp.mn,Method=labs)
rocplotSmall<-ggplot(ROC,aes(y=Sens,x=Sp,shape=Method))+geom_point()+
scale_shape_manual(values=1:10)+
xlab("1-Specificity")+ylab("Sensitivity")+theme_bw()+theme(legend.position="none")
rocplot<-ggplot(ROC,aes(y=Sens,x=Sp,shape=Method))+geom_point()+
scale_shape_manual(values=1:10)+ylim(c(0,1))+xlim(c(0,1))+
xlab("1-Specificity")+ylab("Sensitivity")+theme_bw()+
geom_abline(aes(intercept=0,slope=1),linetype=2)+labs(title="ROC curve")+
theme(strip.text.x = element_text(size=18),axis.title=element_text(size=18))+
theme(axis.text=element_text(size=14))+theme(legend.text=element_text(size=12))+
theme(legend.title=element_text(size=14))
vp <- viewport(width = 0.33, height = 0.33, x = 0.57, y = 0.31)
png("ROCMethods.png",width=15.575,height=12.075,units="cm",res=1200)
print(rocplot)
print(rocplotSmall, vp = vp)
dev.off()
sink("StatisticsOfSensitivitySpec.txt")
cat("Sensitivity\n")
summary(Sens)
cat("\nStandard Deviation\n")
apply(Sens,2,sd)
cat("\nIQR\n")
apply(Sens,2,IQR)
cat("\n\n-------------------------------------------------------\n")
cat("\nSpecificity\n")
summary(Sp)
cat("\nStandard Deviation\n")
apply(Sp,2,sd)
cat("\nIQR\n")
apply(Sp,2,IQR)
sink()
## analyze type I, type 2, FDR and false omission rates from the DE scenarios
library(lme4)
library(boot)
library(ggplot2)
library(plyr)
library(grid)
rm(list=ls()) ## start with a clean slate
FDR<-function(prev,Sens,Spec) 1-Sens*prev/(Sens*prev+(1-Spec)*(1-prev))
FOR<-function(prev,Sens,Spec) 1-Spec*(1-prev)/((1-Sens)*prev+Spec*(1-prev))
funSum<-function(x){
y=mean(x)
se=sd(x)
ymin=quantile(x,0.025)
ymax=quantile(x,.975)
data.frame(y,ymin,ymax)
}
## now load sensitivity (= 1-type 2)
sensDat<-read.csv("Power_Methods.csv")
sensDat.long<-reshape(sensDat,direction="long",varying=list(2:11),timevar = "method",
idvar="id",times=colnames(sensDat)[2:11],v.names="p")
sensDat.long$method<-factor(sensDat.long$method)
## weights are the numbers of tests - logistic regression analyses to find differences
## between methods
weights=rep(286,nrow(sensDat.long))
sensDat.long$Tot=weights
sensDat.long$Suc=round(sensDat.long$p*sensDat.long$Tot)
sensDat.long<-transform(sensDat.long,Fail=Tot-Suc)
fSens<-glmer(cbind(Suc,Fail)~method+(1|Dataset),data=sensDat.long,family=binomial,nAGQ=0)
## now load type I errors
load("FDROutput_TMB.RData")
datRes<-datRes[sapply(datRes,class)=="list"]
## typeI
typeIfun<-function(x,p=0.05) {I(x<=p)}
typeIDat<-sapply(1:length(datRes),function(x) {df=datRes[[x]][[2]];
ret=as.data.frame(apply(df[,-1],2,typeIfun));ret$Dataset=x;ret},simplify=FALSE)
typeIDat<-do.call(rbind,typeIDat)
rownames(typeIDat)<-NULL
typeI.long<-reshape(typeIDat,direction="long",varying=list(1:10),timevar = "method",
idvar="id",times=colnames(typeIDat)[1:10],v.names="p")
typeI.long$method<-factor(typeI.long$method)
typeI.long$p<-as.numeric(typeI.long$p)
typeI.long<-na.omit(typeI.long)
typeI.long$Dataset<-factor(typeI.long$Dataset)
typeI.long<-ddply(typeI.long,.(Dataset,method),summarize,Suc=sum(p),Fail=length(p)-sum(p),Tot=length(p))
ftypeI<-glmer(cbind(Suc,Fail)~method+(1|Dataset),data=typeI.long,family=binomial,nAGQ=0)
## Save results of analyses - for use in the paper
sink("RegressionAnalysisOfTypeIType2.txt")
cat("Type I error (logistic regression)\n")
summary(ftypeI)
cat("\n-----------------------------------------------------------\n")
cat("Type II error (logistic regression)\n")
summary(fSens)
sink()
## now bootstrap to get estimates of type I errors and sensitivities
myEst<-function(.){
logits<-getME(.,"beta")
logits<-logits+c(0,rep(logits[1],length(logits)-1))
inv.logit(logits)
}
set.seed(1234) - ## bootstrap is computational exp
system.time(SensBoot<-bootMer(fSens,myEst,nsim=1000))
system.time(typeIBoot<-bootMer(ftypeI,myEst,nsim=1000))
## create more sensitive labels
labs=c("DESeq2","dss","EBSeq","edgeR","gamlssAD","limmaTMM",
"limma","voomSW","voomTMM","voom")
Sens<-SensBoot[[2]]
Sp<-1-typeIBoot[[2]]
colnames(Sens)<-levels(sensDat.long$method)
colnames(Sp)<-levels(sensDat.long$method)
prev<-seq(0.005,.50,.01)
fdrV<-sapply(prev,function(x) cbind(x,FDR(x,Sens,Sp)),simplify=FALSE)
fdrV<-as.data.frame(do.call(rbind,fdrV))
fdrV$metric="FDR"
forV<-sapply(prev,function(x) cbind(x,FOR(x,Sens,Sp)),simplify=FALSE)
forV<-as.data.frame(do.call(rbind,forV))
forV$metric="FOR"
DR<-rbind(fdrV,forV)
DR<-reshape(DR,direction="long",varying=list(c(2:11)),timevar="Method",idvar="id",
times=colnames(DR)[c(2:11)],v.names="meas")
DR$Method<-factor(DR$Method,labels=labs)
ggplot(DR,aes(x,meas,facets=metric,color=Method))+stat_summary(fun.data=funSum,geom="line")+
facet_wrap(~metric,scales="free",nrow=1)+xlab("% of DE RNAs")+ylab("Metric")+theme_bw()+
scale_x_continuous(breaks=c(0.0,.15,.30,.45))+
theme(strip.text.x = element_text(size=18),axis.title=element_text(size=18))+
theme(axis.text=element_text(size=14))+theme(legend.text=element_text(size=12))+
theme(legend.title=element_text(size=14))
ggsave("FDRFOR.png",width=15.575,height=12.075,units="cm",dpi=1200)
ggplot(DR,aes(x,meas))+stat_summary(fun.data=funSum,geom="smooth")+
facet_grid(metric~Method,scales="free")+xlab("% of DE RNAs")+ylab("Metric")+theme_bw()+
scale_x_continuous(breaks=c(.10,.30,.5))
ggsave("FDRFOR_CI.png",width=12,height=6,dpi=1200,units="in")
Sens.mn<-apply(Sens,2,mean)
Sp.mn<-apply(1-Sp,2,mean)
ROC<-data.frame(Sens=Sens.mn,Sp=Sp.mn,Method=labs)
rocplotSmall<-ggplot(ROC,aes(y=Sens,x=Sp,shape=Method))+geom_point()+
scale_shape_manual(values=1:10)+
xlab("1-Specificity")+ylab("Sensitivity")+theme_bw()+theme(legend.position="none")
rocplot<-ggplot(ROC,aes(y=Sens,x=Sp,shape=Method))+geom_point()+
scale_shape_manual(values=1:10)+ylim(c(0,1))+xlim(c(0,1))+
xlab("1-Specificity")+ylab("Sensitivity")+theme_bw()+
geom_abline(aes(intercept=0,slope=1),linetype=2)+labs(title="ROC curve")+
theme(strip.text.x = element_text(size=18),axis.title=element_text(size=18))+
theme(axis.text=element_text(size=14))+theme(legend.text=element_text(size=12))+
theme(legend.title=element_text(size=14))
vp <- viewport(width = 0.33, height = 0.33, x = 0.57, y = 0.31)
png("ROCMethods.png",width=15.575,height=12.075,units="cm",res=1200)
print(rocplot)
print(rocplotSmall, vp = vp)
dev.off()
sink("StatisticsOfSensitivitySpec.txt")
cat("Sensitivity\n")
summary(Sens)
cat("\nStandard Deviation\n")
apply(Sens,2,sd)
cat("\nIQR\n")
apply(Sens,2,IQR)
cat("\n\n-------------------------------------------------------\n")
cat("\nSpecificity\n")
summary(Sp)
cat("\nStandard Deviation\n")
apply(Sp,2,sd)
cat("\nIQR\n")
apply(Sp,2,IQR)
sink()
