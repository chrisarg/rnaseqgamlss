## analyze the correction factors from the 286 dataset
library(mgcv)
library(gamlss)
library(ggplot2)
library(leiv)
rm(list=ls()) ## start with a clean slate
## get all the correction factors together
load("BiasFactors_286.RData")
off<-Reduce(function(...) merge(...,all=TRUE),corFac)



## conver correction factors to log scale
off[,-1]<-off[,-1]/log(10)
## plot the correction factor analyses

## put histograms on the diagonal
panel.hist <- function(x, ...)
{
  usr <- par("usr"); on.exit(par(usr))
  par(usr = c(usr[1:2], 0, 1.5) )
  h <- hist(x, breaks=30,plot = FALSE)
  breaks <- h$breaks; nB <- length(breaks)
  y <- h$counts; y <- y/max(y)
  rect(breaks[-nB], 0, breaks[-1], y, col = "cyan", ...)
}


## put (absolute) correlations on the upper panels,
## with size proportional to the correlations.
panel.cor <- function(x, y, digits = 2, prefix = "", cex.cor, ...)
{
  usr <- par("usr"); on.exit(par(usr))
  par(usr = c(0, 1, 0, 1))
  r <- abs(cor(x, y,method="pearson"))
  txt <- format(c(r, 0.123456789), digits = digits)[1]
  txt <- paste0(prefix, txt)
  if(missing(cex.cor)) cex.cor <- 0.8/strwidth(txt)
  text(0.5, 0.5, txt, cex = cex.cor * r)
}

## errors in variables regression with the regression equation and 
## the perfect line (intercept of zero and slope one in the graph)
panel.eiv<-function (x, y, col = par("col"), bg = NA, pch = par("pch"), 
                     cex = 1, col.eiv = "red", col.perfect="blue", col.smooth="green",
                     cex.pt=1,lwd=2,span = 1/3, iter = 3, ...) 
{
  points(x, y, pch = pch, col = col, bg = bg, cex = cex.pt)
  ok <- is.finite(x) & is.finite(y)
  if (any(ok)) {
    df<-data.frame(x=x,y=y)
    modr=leiv(y~x,abs.tol=1E-16,subdivisions=1000);
    a=modr@intercept;
    b=modr@slope;
    #g<-gam(y~s(x),data=df)
    #df$p<-predict(g,type="response")
    abline(a=a,b=b,col=col.eiv,lwd=lwd)
    abline(a=0,b=1,col=col.perfect,lwd=lwd)
    # points(df$x,df$p,col=col.smooth)
  }
}

png("BivariateCorrplot_286.png",width=8,height=8,units="in",res=600,pointsize=10)
pairs(~off.m.01+off.m.1+off.m.10+off.m.100,data=off,lower.panel = panel.eiv, 
      upper.panel = panel.cor,diag.panel=panel.hist,cex.pt=0.35,
      labels=c("0.1 fmole/4N-4Nv3\nPEG15","1 fmoles/4N-4Nv3\nPEG15","10 fmoles/4N-4Nv3\nPEG15",
               "100 fmoles/4N-4Nv3\nPEG15"),main="Bias factor (log-scale, base10)",line.main=3,cex.main=1.7)
dev.off()