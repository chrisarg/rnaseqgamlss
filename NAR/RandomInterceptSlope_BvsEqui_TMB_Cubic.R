## random factors to assess differential expression changes in GAMLSS
## restrict attention to half of the miRNAs ie those in the -3, -1 range
## cubic root transformation
library(plyr)
library(TMB)
library(mclust)
rm(list=ls()) ## start with a clean slate
dyn.load(dynlib("LQNO_DE"))
dyn.load(dynlib("NBI_DE"))
load("286v2.RData") ## load the v2, to generate the v2 image files


######################################################################################
## Prepare data for GAMLSS
datRat<-subset(dat286.long,(Series=="RatioB" | Series =="Equi") & 
	Amount=="100 fmoles" )
datRat$SampleID<-factor(datRat$SampleID)
datRat$Series<-factor(datRat$Series)
datRat$miRNA<-factor(datRat$miRNA)
datRat<-datRat[order(datRat$SampleID,datRat$miRNA),]
attr(datRat$Amount,"row.names")<-NULL
######################################################################################
CubRoot<-data.frame(miRNA=levels(datRat$miRNA),p=NA,GC=NA)
for (i in 1:nlevels(datRat$miRNA)) {
  df<-subset(datRat,miRNA==CubRoot$miRNA[i])
  tt <-t.test(I(reads^(1/3))~Series,data=df)
  CubRoot$p[i]<-tt$p.value
  CubRoot$GC[i]<-3*(log(tt$estimate[2],10)-log(tt$estimate[1],10))
}



## now fit the fixed effect model for the p-values. 
u_X<-as.numeric(factor(datRat$miRNA))
u_G<-as.numeric(factor(datRat$Series))
y=datRat$reads
X<-model.matrix(~Series,data=datRat)
sigmu=rep(1,max(u_G))
sigsig=rep(1,max(u_G))
b=rep(0,ncol(X))
s_b=rep(0,ncol(X))
u_m=matrix(0,max(u_X),max(u_G))
u_s=matrix(0,max(u_X),max(u_G))
obj.TMB<-MakeADFun(data=list(y=y,X=X,u_X=u_X,u_G=u_G),
                   parameters=list(b=b,s_b=s_b,u_m=u_m,u_s=u_s,
                                   sigmu=sigmu,sigsig=sigsig),
                   DLL="NBI_DE",random=c("u_m","u_s"),hessian=TRUE,silent=TRUE,
                   method="BFGS",random.start=expression(last.par[random]),
                   ADReport=TRUE)
t3<-Sys.time()
f.TMB<-nlminb(obj.TMB$par,obj.TMB$fn,obj.TMB$gr,
              control=list(eval.max=10000,iter.max=10000),lower=-30,upper=30)
t4<-Sys.time()
print(t4-t3)
t5<-Sys.time()
rep<-sdreport(obj.TMB)
t6<-Sys.time()
print(t6-t5)
## transform the coefficient and the covarience matrix to obtain differential
## expression levels in the second group. 
dummy<-summary(rep,"report")[1:nlevels(datRat$miRNA),"Estimate"]

gamlssfit2<-data.frame(miRNA=levels(datRat$miRNA),gamlssAD=log(exp(dummy),10))

gamlssADP<-summary(rep,"report",p.value=TRUE)[1:nlevels(datRat$miRNA), 4]
gamlssADP<-data.frame(miRNA=levels(datRat$miRNA), gamlssADP=gamlssADP)
gamlssADP<-merge(gamlssADP,gamlssfit2)
gamlssADP<-merge(gamlssADP,CubRoot)

resP.long<-reshape(subset(gamlssADP,select=c(miRNA,gamlssADP,p)),
	direction="long",varying=list(2:3),timevar = "method",
                   idvar="miRNA",times=c("gamlssADP","CRp"),v.names="p")
resP.long$Dataset<-"BvsEquiv2"
write.csv(resP.long,file="pvalue_BvsEqui_CRv2.csv",row.names=F)

## RMS calculations
compr<-unique(subset(datRat,select=c(miRNA,Dilution),Series!="Equi"))
compr<-ddply(compr,.(Dilution),summarize,n=length(miRNA))
compr<-100*compr$n/sum(compr$n)
itc<-log((c(1,.1,.01,.001)*100/1.111)/compr,10)## xintercepts of true values
truth<-unique(subset(datRat,select=c(miRNA,Dilution),Series!="Equi"))
tr<-data.frame(Dilution=unique(truth$Dilution),trVal=rev(itc))
truth<-merge(truth,tr)

gamlssADP<-merge(gamlssADP,truth)
RMSdf.long<-reshape(gamlssADP[,c(1,3,5,7)],direction="long",varying=list(2:3),timevar = "method",
                    idvar="miRNA",times=colnames(gamlssADP)[c(3,5)],v.names="meas")

rms<-ddply(RMSdf.long,.(trVal,method),.fun=function(x) sqrt(mean((x$meas-x$trVal)^2)))
rms$y<-c(0.6,0.55)
rms$col<-c(1,2)
names(rms)[3]<-"RMS"
newdata<-seq(-3.5,1.5,.01)


png("RandomInterceptSlope_286v2_SeriesBEqui_CR.png",width=4,heigh=4,units="in",res=1200,pointsize=8)
den.AD<-densityMclust(gamlssADP$gamlssAD,G=1:10,modelName="V")
den.CR<-densityMclust(gamlssADP$GC,G=1:10,modelName="V")
plot(newdata,dens(den.AD$modelName,data=newdata,parameters=den.AD$parameters),
     lwd=2,main="",sub="",xlab="",type="l",ylab="Density",ylim=c(0,0.8))
lines(newdata,dens(den.CR$modelName,data=newdata,parameters=den.CR$parameters),col="red",lwd=2)
abline(v=itc,lty=2)
text(rms$trVal-0.6,rms$y,formatC(rms$RMS,dig=2,format="f"),col=rms$col)
legend(legend = c("GAMLSS","CR"),text.col=c(1,2),x=c(.5,.5),y=c(0.60,0.55),bty="n")
dev.off()