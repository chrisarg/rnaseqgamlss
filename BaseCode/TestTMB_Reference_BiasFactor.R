library(gamlss)
library(TMB)
library(plyr)
library(metafor)
library(leiv) ## errors in variable regression facilities
rm(list=ls()) ## start with a clean slate
## test TMB drivers using the 286 data
load("286.RData") 
source("LQNO.R")


dyn.load(dynlib("LQNO_random"))
dyn.load(dynlib("NBI_random"))
## function to obtain the standard errors of the correction
## factors
pred.fun<-function(x) {
  dummy<-predict(x[[1]],type="terms",se.fit=TRUE)
  d<-data.frame(miRNA=x[[2]]$miRNA,m=dummy$fit[,"random(miRNA)"],s=dummy$se.fit[,"random(miRNA)"])
  d<-ddply(d,.(miRNA),summarize,m=mean(m),s=mean(s))
  d
}



## gets the correction factors
corrfact<-function(fit,data) {
  pred<-predictAll(fit,type="terms",data=data)
  off.m<-pred$mu[,"random(miRNA)"]
  off.s<-pred$sigma[,"random(miRNA)"]
  off<-aggregate(cbind(off.m,off.s),by=list(data$miRNA),FUN=mean)
  names(off)[1]<-"miRNA"
  off
}

## gets the correction factors - fixed effects
fcorrfact<-function(fit,data) {
  pred<-predictAll(fit,type="terms",data=data)
  off.m<-pred$mu[,"miRNA"]
  off.s<-pred$sigma[,"miRNA"]
  off<-aggregate(cbind(off.m,off.s),by=list(data$miRNA),FUN=mean)
  names(off)[1]<-"miRNA"
  off
}

## load the data and select a few miRNAs

datRat<-subset(dat286.long,(Series=="Equi") & Amount=="100 fmoles")
dat<-subset(datRat,is.element(miRNA,levels(datRat$miRNA)[1:286]))
dat$miRNA<-factor(dat$miRNA)
dat$SampleID<-factor(dat$SampleID)
dat$Series<-factor(dat$Series)

## obtain the bias correction factors

f.GM.LQNO<-gamlss(reads~SampleID+random(miRNA),
                  sigma.fo=~SampleID+random(miRNA),
                  data=dat,
                  family="LQNO",
                  control=gamlss.control(n.cyc=5000,gd.tol=Inf),
                  i.control=glim.control(cc = 0.001, cyc = 100,  glm.trace = FALSE, 
                                         bf.cyc = 1000))

f.GM.NBI<-gamlss(reads~SampleID+random(miRNA),
                  sigma.fo=~SampleID+random(miRNA),
                  data=dat,
                  family="NBI",
                  control=gamlss.control(n.cyc=5000,gd.tol=Inf),
                 i.control=glim.control(cc = 0.001, cyc = 100,  glm.trace = FALSE, 
                                        bf.cyc = 1000))
## bias factors and their standard errors
## from the 2 gamlss models
br.GM.LQNO<-pred.fun(list(f.GM.LQNO,dat))
br.GM.NBI<-pred.fun(list(f.GM.NBI,dat))

corr.GM.LQNO<-corrfact(f.GM.LQNO,dat)
corr.GM.NBI<-corrfact(f.GM.NBI,dat)
## now to fit this model

nID<-nlevels(dat$SampleID)
nmiR<-nlevels(factor(dat$miRNA))
u_X<-as.numeric(factor(dat$miRNA))
X<-model.matrix(~SampleID,data=dat)
obj.TMB.LQNO<-MakeADFun(data=list(y=dat$reads,X=X,
                                u_X=u_X,
                                off_m=rep(0,nmiR),
                                off_s=rep(0,nmiR)),
                        parameters=list(b=rep(0,ncol(X)),
                                s_b=rep(0,ncol(X)),
                                u_m=rep(0,nmiR),
                                u_s=rep(0,nmiR),
                                sigmu=1.0,
                                sigsig=1.0),
                        DLL="LQNO_random",
                        random=c("u_m","u_s"),
                        hessian=TRUE,
                        silent=TRUE,
                        method="BFGS",
                        random.start=expression(last.par[random]))
f.TMB.LQNO<-nlminb(obj.TMB.LQNO$par,obj.TMB.LQNO$fn,obj.TMB.LQNO$gr)
rep<-sdreport(obj.TMB.LQNO)

## get the mean bias correction factors
br.TMB.LQNO=data.frame(miRNA=levels(dat$miRNA),
                       m.TMB=summary(rep, "random")[1:nmiR, "Estimate"],
  m.se=summary(rep, "random")[1:nmiR, "Std. Error"])
## get the sd bias correction factors

sr.TMB.LQNO=data.frame(miRNA=levels(dat$miRNA),
                       m.TMB=summary(rep, "random")[(nmiR+1):(2*nmiR), "Estimate"],
                       m.se=summary(rep, "random")[(nmiR+1):(2*nmiR), "Std. Error"])
## NBI
obj.TMB.NBI<-MakeADFun(data=list(y=dat$reads,X=X,
                                 u_X=u_X,
                                 off_m=rep(0,nmiR),
                                 off_s=rep(0,nmiR)),
                       parameters=list(b=rep(0,ncol(X)),
                                       s_b=rep(0,ncol(X)),
                                       u_m=rep(0,nmiR),
                                       u_s=rep(0,nmiR),
                                       sigmu=1.0,
                                       sigsig=1.0),
                       DLL="NBI_random",
                       random=c("u_m","u_s"),
                       hessian=TRUE,
                       silent=TRUE,
                       method="BFGS",
                       random.start=expression(last.par[random]))

f.TMB.NBI<-nlminb(obj.TMB.NBI$par,obj.TMB.NBI$fn,obj.TMB.NBI$gr)
rep<-sdreport(obj.TMB.NBI)

## get the mean bias correction factors
br.TMB.NBI=data.frame(miRNA=levels(dat$miRNA),
                      m.TMB=summary(rep, "random")[1:nmiR, "Estimate"],
                      m.se=summary(rep, "random")[1:nmiR, "Std. Error"])
## get the sd bias correction factors
nmiRNA<-nlevels(dat$miRNA)
sr.TMB.NBI=data.frame(miRNA=levels(dat$miRNA),
                      m.TMB=summary(rep, "random")[(nmiR+1):(2*nmiR), "Estimate"],
                      m.se=summary(rep, "random")[(nmiR+1):(2*nmiR), "Std. Error"])

## compare correction factors between and within
## the gamlss and gamlssAD implementations
## Near identify for the mu submodels
par(mfrow=c(2,2))
plot(br.GM.LQNO$m,br.TMB.LQNO$m.TMB,xlab="gamlss",ylab="gamlssAD",
     main=expression(paste(mu, " model, LQNO")))
abline(a=0,b=1,col="red",lwd=2)
plot(br.GM.NBI$m,br.TMB.NBI$m.TMB,xlab="gamlss",ylab="gamlssAD",
     main=expression(paste(mu, " model, NBI")))
abline(a=0,b=1,col="red",lwd=2)
plot(br.TMB.NBI$m.TMB,br.TMB.LQNO$m.TMB,xlab="NBI",ylab="LQNO",
     main=expression(paste(mu, " model, gamlssAD")))
abline(a=0,b=1,col="red",lwd=2)
plot(br.GM.NBI$m,br.GM.LQNO$m,xlab="NBI",ylab="LQNO",
     main=expression(paste(mu, " model, gamlss")))
abline(a=0,b=1,col="red",lwd=2)


## less so for the phi submodels - there are discernible effects of 
## optimization and distributional family here
plot(corr.GM.LQNO$off.s,sr.TMB.LQNO$m.TMB,xlab="gamlss",ylab="gamlssAD",
     main=expression(paste(phi, " model, LQNO")))
abline(a=0,b=1,col="red",lwd=2)
plot(corr.GM.NBI$off.s,sr.TMB.NBI$m.TMB,xlab="gamlss",ylab="gamlssAD",
     main=expression(paste(phi, " model, NBI")))
abline(a=0,b=1,col="red",lwd=2)
plot(sr.TMB.NBI$m.TMB,sr.TMB.LQNO$m.TMB,xlab="NBI",ylab="LQNO",
     main=expression(paste(phi, " model, gamlssAD")))
abline(a=0,b=1,col="red",lwd=2)
plot(corr.GM.NBI$off.s,corr.GM.LQNO$off.s,xlab="NBI",ylab="LQNO",
     main=expression(paste(phi, " model, gamlss")))
abline(a=0,b=1,col="red",lwd=2)

## plot estimates of mu and phi to illustrate the internal consistency checks
## to carry out the regression analyses for the consistency checks we have to put together
## the estimates in a single data frame (this is already the case for the reference impl)
## now to bring together the correction factors from the TMB implementations
corr.TMB.NBI<-merge(br.TMB.NBI[,1:2],sr.TMB.NBI[,1:2],by="miRNA")
corr.TMB.LQNO<-merge(br.TMB.LQNO[,1:2],sr.TMB.LQNO[,1:2],by="miRNA")
names(corr.TMB.LQNO)[2:3]<-names(corr.TMB.NBI)[2:3]<-c("off.m","off.s")
fit1<-leiv(off.s~off.m,data=corr.GM.NBI)
fit2<-leiv(off.s~off.m,data=corr.GM.LQNO)
fit3<-leiv(off.s~off.m,data=corr.TMB.NBI)
fit4<-leiv(off.s~off.m,data=corr.TMB.LQNO)

par(mfrow=c(2,2))

plot(corr.GM.NBI$off.m,corr.GM.NBI$off.s,xlab=expression(mu),ylab=expression(phi),main="gamlss, NBI")
abline(a=0,b=-1,col="red",lwd=2)
abline(a=fit1@intercept,b=fit1@slope,lwd=2)
text(2,2.5e-6,formatC(cor(corr.GM.NBI$off.m,corr.GM.NBI$off.s),3),font=2)

plot(corr.GM.LQNO$off.m,corr.GM.LQNO$off.s,xlab=expression(mu),ylab=expression(phi),main="gamlss, LQNO")
abline(a=0,b=-1,col="red",lwd=2)
abline(a=fit2@intercept,b=fit2@slope,lwd=2)
text(2,1.8,formatC(cor(corr.GM.LQNO$off.m,corr.GM.LQNO$off.s),3),font=2)

plot(corr.TMB.NBI$off.m,corr.TMB.NBI$off.s,xlab=expression(mu),ylab=expression(phi),main="gamlssAD, NBI")
abline(a=0,b=-1,col="red",lwd=2)
abline(a=fit3@intercept,b=fit3@slope,lwd=2)
text(2,1.8,formatC(cor(corr.TMB.NBI$off.m,corr.TMB.NBI$off.s),3),font=2)

plot(br.TMB.LQNO$m.TMB,sr.TMB.LQNO$m.TMB,xlab=expression(mu),ylab=expression(phi),main="gamlssAD, LQNO")
abline(a=0,b=-1,col="red",lwd=2)
abline(a=fit4@intercept,b=fit4@slope,lwd=2)
text(2,1.1,formatC(cor(br.TMB.LQNO$m.TMB,sr.TMB.LQNO$m.TMB),3),font=2)

## apply correction factors
dat2<-subset(dat286.long,Series=="Equi" &  Amount=="10 fmoles")
dat2<-merge(dat2,corr.GM.LQNO)
dat2$SampleID<-factor(dat2$SampleID)
dat2$miRNA<-factor(dat2$miRNA)
f.GM.LQNO.corr<-gamlss(reads~SampleID+random(miRNA)+offset(off.m),
                  sigma.fo=~SampleID+random(miRNA)+offset(off.s),
                  data=dat2,
                  family="LQNO",
                  control=gamlss.control(n.cyc=5000,gd.tol=Inf))


br.GM.LQNO.corr<-pred.fun(list(f.GM.LQNO.corr,dat2))

## now do the same with TMB
nID<-nlevels(dat2$SampleID)
nmiR<-nlevels(factor(dat2$miRNA))
u_X<-as.numeric(factor(dat2$miRNA))
X<-model.matrix(~SampleID,data=dat2)
obj.TMB.NBI.corr<-MakeADFun(data=list(y=dat2$reads,X=X,
                                  u_X=u_X,
                                  off_m=corr.TMB.NBI[,"off.m"],
                                  off_s=corr.TMB.NBI[,"off.s"]),
                        parameters=list(b=rep(0,ncol(X)),
                                        s_b=rep(0,ncol(X)),
                                        u_m=rep(0,nmiR),
                                        u_s=rep(0,nmiR),
                                        sigmu=1.0,
                                        sigsig=1.0),
                        DLL="NBI_random",
                        random=c("u_m","u_s"),
                        hessian=TRUE,
                        silent=TRUE,
                        method="BFGS",
                        random.start=expression(last.par[random]))

f.TMB.NBI.corr<-nlminb(obj.TMB.NBI.corr$par,obj.TMB.NBI.corr$fn,obj.TMB.NBI.corr$gr)
rep2<-sdreport(obj.TMB.NBI.corr)
br.TMB.NBI.corr=data.frame(miRNA=levels(dat2$miRNA),
                       m.TMB=summary(rep2, "random")[1:nlevels(dat2$miRNA), "Estimate"],
                       m.se=summary(rep2, "random")[1:nlevels(dat2$miRNA), "Std. Error"])

par(mfrow=c(1,1))
plot(br.GM.LQNO.corr$m,br.TMB.NBI.corr$m.TMB,xlab="gamlss",ylab="gamlssAD",main="Bias Corrected Values")
abline(a=0,b=1,lwd=2,col="red")
