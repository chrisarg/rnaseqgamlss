// Illustrate map feature of TMB to perform likelihood ratio tests on a nRNAged array dataset.
#include <TMB.hpp>
// NBI regression family for the estimation of bias factors

template<class Type>
Type objective_function<Type>::operator() ()
{
	DATA_VECTOR(y);	// read counts
	DATA_MATRIX(X);	// library membership for each read count
	DATA_FACTOR(u_X); // RNA membership for each read count
	DATA_VECTOR(off_m); // correction factor for mean
	DATA_VECTOR(off_s); // correction factor for sigma
	PARAMETER_VECTOR(b);  // fixed effects of library on mean parameter
	PARAMETER_VECTOR(s_b); // fixed effects of library on sigma parameter
	PARAMETER_VECTOR(u_m); // expression of microRNA relative to the mean (mean submodel)
	PARAMETER_VECTOR(u_s); // expression of microRNA relative to the mean (sigma submodel)
	PARAMETER(sigmu);  // standard deviation of the random effects (u_m) on the mean submodel
	PARAMETER(sigsig); // standard deviation of the random effects (u_s) on the sigma submodel

	Type res = 0;  // negative log-likelihood 

	vector<Type> eta(y.size());   // linear predictor of the mean submodel
	vector<Type> logsigma(y.size()); // linear predictor of the sigma submodel
	long int nRNA = u_m.size();		// number of RNAs in the libraries
	eta = X*b;
	logsigma = X*s_b;


	// linear predictor contribution for each miRNA
	// includes offset for bias correction
	for (long int i = 0; i<y.size(); i++){
		eta[i] += (u_m[u_X[i] - 1] + off_m[u_X[i] - 1]);
		logsigma[i] += (u_s[u_X[i] - 1] + off_s[u_X[i] - 1]);
	}
	// construction of negative log-likelihood
  for(long int i=0;i<y.size();i++){
    res -= dnbinom(y[i],exp(-logsigma[i]),1.0/(1.0+exp(logsigma[i]+eta[i])),true);
  }
  // random effect contribution to the negative log-likelihood
  for(long int i=0;i<nRNA;i++){
    res -= dnorm(u_m[i],Type(0.0),sigmu,true);
    res -= dnorm(u_s[i],Type(0.0),sigsig,true);
  }
  return res;
}
